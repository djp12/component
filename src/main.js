import Vue from 'vue'
import App from './App.vue'
import router from './router'
// import CountButton from "./components/CountButton/CountButton.vue"


// Vue.component("CountButton",CountButton)

Vue.config.productionTip = false

// Vue.use(router)
new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
