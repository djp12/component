import Vue from 'vue';
import VueRouter from "vue-router";
import Home from "../views/home/homeIndex"

Vue.use(VueRouter)
export default new VueRouter({
  routes: [{
      path: '/home',
      name: 'home',
      component: Home
  },{
    path: '/login',
    name: 'login',
    //  // component 后边跟的是一个文件路径位置，不是一个字符串，所以要用import 引入这个路径
    component: ()=> import("../views/login/loginIndex.vue")
  }]
})